package controller;

import model.entity.Deposit;
import model.business.InterestBis;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

class OutputParser {
    private static OutputParser outputParser = new OutputParser();

    public OutputParser() {
    }

    public static OutputParser getInstance() {
        return outputParser;
    }

    public void writeToFile(List<Deposit> depositList) {
        try (FileWriter fileWriter = new FileWriter("src\\resources\\output.txt", false);
             PrintWriter printWriter = new PrintWriter(fileWriter)) {
            depositList = InterestBis.getInstance().sortInterest(depositList);

            for (Deposit deposit : depositList) {
                printWriter.println(deposit.getCustomerNumber() + "#" + deposit.payedInterest());
            }

            System.out.println("نتایج در output.txt ذخیره شدند");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
