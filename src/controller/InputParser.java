package controller;

import model.entity.Deposit;
import model.entity.Longterm;
import model.entity.Qarz;
import model.entity.ShortTerm;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InputParser {
    public InputParser() {
        readFile();
    }

    private List<Deposit> depositList = new ArrayList<>();

    private void readFile() {
        try {
            System.out.println("شروع فرایند بازیابی اطلاعات از فایل ورودی");
            File xmlFile = new File("src\\resources\\input.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("deposit");

            for (int i = 0; i < nList.getLength(); i++) {
                Deposit deposit = null;
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    try {
                        BigDecimal depositBalance = new BigDecimal(eElement.getElementsByTagName("depositBalance").item(0).getTextContent());
                        if (depositBalance.compareTo(new BigDecimal(0)) == -1)
                            throw new Exception("Deposit Balance is less than 0");
                        long durationInDays = Long.valueOf(eElement.getElementsByTagName("durationInDays").item(0).getTextContent());
                        if (durationInDays < 0)
                            throw new Exception("Duration In Days is less than 0");
                        String customerNumber = eElement.getElementsByTagName("customerNumber").item(0).getTextContent();
                        if (customerNumber.equals(""))
                            throw new Exception("customer number is null");
                        switch (eElement.getElementsByTagName("depositType").item(0).getTextContent()) {
                            case "ShortTerm":
                                deposit = new ShortTerm(customerNumber, depositBalance, durationInDays);
                                break;
                            case "LongTerm":
                                deposit = new Longterm(customerNumber, depositBalance, durationInDays);
                                break;
                            case "Qarz":
                                deposit = new Qarz(customerNumber, depositBalance, durationInDays);
                                break;
                            default:
                                throw new Exception("Deposit type is not valid");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (deposit != null) {
                    depositList.add(deposit);
                }

            }

            OutputParser.getInstance().writeToFile(depositList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
