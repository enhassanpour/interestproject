package model.business;

import model.entity.Deposit;

import java.util.*;
import java.util.stream.Collectors;

public class InterestBis {
    private static InterestBis interestBis = new InterestBis();

    InterestBis() {
    }

    public static InterestBis getInstance() {
        return interestBis;
    }

    public List<Deposit> sortInterest(List<Deposit> depositList) {
        List<Deposit> depositListSorted = new ArrayList<>();
        try {
            depositListSorted = depositList.stream().sorted(Comparator.comparingLong(Deposit::payedInterest)).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("مرتب سازی مقادیر انجام شد.");
        return depositListSorted;
    }
}
