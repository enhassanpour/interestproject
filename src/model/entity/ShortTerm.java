package model.entity;

import java.math.BigDecimal;

public class ShortTerm extends Deposit implements TermInterest {
    private final static double interestRate = 0.20;

    public ShortTerm(String customerNumber, BigDecimal depositBalance, Long durationInDays) {
        super(customerNumber, depositBalance, durationInDays);
    }

    public Long payedInterest() {
        return payedInterest(interestRate, super.getDepositBalance(), super.getDurationDays());
    }
}
