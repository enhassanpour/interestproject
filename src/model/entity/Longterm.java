package model.entity;

import java.math.BigDecimal;

public class Longterm extends Deposit implements TermInterest {
    private final static double interestRate = 0.20;

    public Longterm(String customerNumber, BigDecimal depositBalance, long durationInDays) {
        super(customerNumber, depositBalance, durationInDays);
    }

    public Long payedInterest() {
        return payedInterest(interestRate, super.getDepositBalance(), super.getDurationDays());
    }
}
