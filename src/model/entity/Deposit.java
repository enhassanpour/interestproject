package model.entity;

import java.math.BigDecimal;

public class Deposit {
    private String customerNumber;
    private BigDecimal depositBalance;
    private Long durationDays;

    public Deposit(String customerNumber, BigDecimal depositBalance, Long durationDays) {
        this.customerNumber = customerNumber;
        this.depositBalance = depositBalance;
        this.durationDays = durationDays;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public Deposit setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
        return this;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public Deposit setDepositBalance(BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
        return this;
    }

    public Deposit setDurationDays(Long durationDays) {
        this.durationDays = durationDays;
        return this;
    }

    public Long getDurationDays() {
        return durationDays;
    }

    public Long payedInterest() {
        return new Long(0);
    }
}
