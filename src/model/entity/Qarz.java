package model.entity;

import java.math.BigDecimal;

public class Qarz extends Deposit implements TermInterest {
    final static double interestRate = 0;

    public Qarz(String customerNumber, BigDecimal depositBalance, Long durationInDays) {
        super(customerNumber, depositBalance, durationInDays);
    }

    public Long payedInterest() {
        return 0L;
    }
}
