package model.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface TermInterest {
    default Long payedInterest(double interestRate, BigDecimal depositBalance, long durationInDays) {
        final BigDecimal divNumber = new BigDecimal(36500);
        BigDecimal resultValue = depositBalance.multiply(new BigDecimal(interestRate));
        resultValue = resultValue.multiply(new BigDecimal(durationInDays));
        resultValue = resultValue.divide(divNumber, 3, RoundingMode.CEILING);
        return resultValue.longValue();
    }
}
